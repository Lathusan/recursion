package com.recursion;

public class RecursionDemo {

	public static void main(String[] args) {
		RecursionDemo re = new RecursionDemo();
		re.print(3);
	}

	public int print(int n) {
		while (n >= 0) {
			if (n == 0) {
				System.out.println(n);
			} else {
				System.out.println(n);
			}
			n--;
		}
		return (n);
	}
}
